import LoginPage from '../pages/login.vue';
import RegisterPage from '../pages/register.vue';
import HomePage from '../pages/home.vue';
import PrivacyPage from '../pages/privacy.vue';
import AboutPage from '../pages/about.vue';
import ProfilePage from '../pages/profile.vue';
import NewGamePage from '../pages/new-game.vue';
import GamePage from '../pages/game.vue';
import GameEditPage from '../pages/game-edit.vue';

import NotFoundPage from '../pages/404.vue';

var routes = [
  {
    path: '/login',
    component: LoginPage,
  },
  {
    path: '/register',
    component: RegisterPage,
  },
  {
    path: '/',
    component: HomePage,
  },
  {
    path: '/about/',
    component: AboutPage,
  },
  {
    path: '/privacy/',
    component: PrivacyPage,
  },
  {
    path: '/profile/',
    component: ProfilePage,
  },
  {
    path: '/new-game/',
    component: NewGamePage,
  },
  {
    path: '/game/:id',
    async async(routeTo, routeFrom, resolve, reject) {
      this.app.dialog.preloader();
      try {
        const gameData = await this.app.request.promise.json(`${process.env.API_URL}/games/${routeTo.params.id}`);
        resolve({
          component: GamePage
        }, {
          props: {
            gameData: gameData.data
          }
        });
      } catch ({ xhr }) {
        const response = JSON.parse(xhr.response);
        this.app.dialog.alert(response.message);
        reject();
      };
      this.app.dialog.close();
    }
  },
  {
    path: '/game/:id/edit',
    async async(routeTo, routeFrom, resolve, reject) {
      this.app.dialog.preloader();
      try {
        const gameData = await this.app.request.promise.json(`${process.env.API_URL}/games/${routeTo.params.id}`);
        resolve({
          component: GameEditPage
        }, {
          props: {
            gameData: gameData.data
          }
        });
      } catch ({ xhr }) {
        const response = JSON.parse(xhr.response);
        this.app.dialog.alert(response.message);
        reject();
      };
      this.app.dialog.close();
    }
  },

  // Default error handler
  {
    path: '(.*)',
    component: NotFoundPage,
  },
];

export default routes;
